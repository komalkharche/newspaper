<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'newspaper' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '7VAn8ht,iYFODz#a2jf<gt.CM,Y$Q.z*Q#c`g50Yxs4GL,sa$g5h{toR-LqU,9O2' );
define( 'SECURE_AUTH_KEY',  '2QTaW]97&AP1$d8;p-5Lp71ZM(!22!t`?{-wI?,hw}+T z.LhW77?RB]*&Ewo>aM' );
define( 'LOGGED_IN_KEY',    'psO-p#7XHUfa*+c  M;|K*~t8MXT4,)CIV1JGO[%$A[J1)aZNBxuR1.o)o@_%R>Z' );
define( 'NONCE_KEY',        '9n3)2:8RG/ubZ@n~-7FtNgI0UW`-onw!~g__=Z+5sc/xj|6Xv1V: i`RS}0sRBEs' );
define( 'AUTH_SALT',        ')eB!7(~n;#4*q_Nu!RH-M{0M<!Qgao[o]UQfMI*rOCrs],YB0UICK%6kI|Aif.Vf' );
define( 'SECURE_AUTH_SALT', 'n$BSkaQ6B6z;dz14d>cgitjbOtY6v>QCGmbq;yGC{@&KSXIJ/:8dmr9Vz)4X|qt^' );
define( 'LOGGED_IN_SALT',   'v%2&qd!uA8%hj:Pa5&Lr)R-BOK=i-bUV/_j`}=s*[QZlMcQ5q@t~7w&[5q8 6EC?' );
define( 'NONCE_SALT',       ')53{[^r6J7WQ21Jd-dOHZt&<[kXqF#wz+y[7Wr4d*s$Qk{Lt%kIlG^m.1&,qkP^P' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
